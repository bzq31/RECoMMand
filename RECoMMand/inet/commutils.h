#ifndef _H_ZUS_COMMON_BASEUTILS_COMMUTILS_H
#define _H_ZUS_COMMON_BASEUTILS_COMMUTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/un.h>

int tcp_server(int port);
int tcp_client(const char* hostIp, int port);
int tcp_write(int fd, const char* buffer, int len);
int tcp_read(int fd, const char* buffer, int len);
void  itoa   (   unsigned   long   val,   char   *buf,   unsigned   radix   );

int fd_write_buf(int fd, const char *buf, int len);
int fd_read_buf(int fd, char *buf, int maxLen);

int fd_send_msg(int fd, const char *msg, int len);
int fd_recv_msg(int fd, char *msg, int maxLen);

#endif
