#RECoMMand
----

RECoMMand(Remote Cooperate Command)是远程协助命令行的缩写，可以在MAC上远程协助进行命令行操作。

目前功能还不完善，但是已经可以正常流程操作了，大家一起来开发吧。


#使用
----
移动recommand到/usr/bin/

```
sudo mv recommand /usr/bin/
```

需要协助者使用，端口可以自设:

```
recommand 8787
```

协助者使用:

```
recommand 需要协助者IP 8787
```

查看帮助信息:

```
recommand
```



