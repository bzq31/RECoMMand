//
//  shell_processor.h
//  CoMmand
//
//  Created by Zhiqiang Bao on 15-1-16.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#ifndef __CoMmand__shell_processor__
#define __CoMmand__shell_processor__

#include <stdio.h>

/**
 *  运行shell命令，并输出到对应的buf
 *
 *  @param cmdstring shell命令
 *  @param buf       储存输出的buffer
 *  @param len       buffer的最大长度
 *
 *  @return -1失败 0正确
 */
int system2buf(char* cmdstring, char* buf, int len);

#endif /* defined(__CoMmand__shell_processor__) */
