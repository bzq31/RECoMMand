//
//  NSTaskProcessor.m
//  CoMmand
//
//  Created by Zhiqiang Bao on 15-1-16.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "NSTaskProcessor.h"

@interface NSTaskProcessor ()

@property (nonatomic, strong) NSTask *task;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSPipe *writePipe;

@end

@implementation NSTaskProcessor

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    NSTask *task = [[NSTask alloc] init];
    
    task.launchPath = @"/bin/sh";
    
    self.task = task;
    
    self.writePipe = [NSPipe pipe];
    self.readPipe = [NSPipe pipe];
    
    self.task.standardInput = self.writePipe.fileHandleForReading;
    self.task.standardOutput = self.readPipe.fileHandleForWriting;
    self.task.standardError = self.readPipe.fileHandleForWriting;
    
    [self.task launch];
}

- (void)processCommand:(char *)cmd
{
    [self.writePipe.fileHandleForWriting writeData:[NSData dataWithBytes:cmd length:strlen(cmd)]];
}

@end
