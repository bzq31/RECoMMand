//
//  NSTaskProcessor.h
//  CoMmand
//
//  Created by Zhiqiang Bao on 15-1-16.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NSTaskProcessor : NSObject

@property (nonatomic, strong) NSPipe *readPipe;

- (void)processCommand:(char *)cmd;

@end
